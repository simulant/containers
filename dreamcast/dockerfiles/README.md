Please add Dockerfiles to this directory *in subfolders* with the following rules.

 - Folders should be named: `gcc_XXX_newlib_YYY`
 - If additional things have changed since KOS upstream, append those in the same format
 - If this is a Simulant file with extra tools, suffix `_with_tools`
 - Any required patches should be added to the 'patches' directory in the folder above